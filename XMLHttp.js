function Login(){
    var xmlhttp = window.XMLHttpRequest ? new XMLHttpRequest() : new
    ActiveXObject('Microsoft.XMLHTTP');
    
    var obj = {
        name: document.getElementById('username').value,
        password: document.getElementById('password').value
    };
    var pairs = []
    for (var key in obj) {
        pairs.push(key + '=' + obj[key])
    }
    var querystring = pairs.join('&')
    xmlhttp.open("POST", "http://erp_test.admin.htwig.com/api/authorizations", true);
    xmlhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xmlhttp.send(querystring);  // 要发送的参数，要转化为json字符串发送给后端，后端就会接受到json对象
    xmlhttp.addEventListener('readystatechange', function () {
        if(xmlhttp.readyState == 4 && xmlhttp.status == 200){
            console.log(JSON.parse(xmlhttp.responseText), xmlhttp.responseText.code)
            if( JSON.parse(xmlhttp.responseText).code == 0 ) {
                // console.log(JSON.parse(xmlhttp.responseText));
                window.localStorage.setItem("access_token", JSON.stringify(JSON.parse(xmlhttp.responseText).data.access_token));
                window.location.href='CustomerProfile.html';
            } else {
                let message = document.getElementById('showDialog');
                let meText = document.getElementById('prompt');
                meText.innerHTML = JSON.parse(xmlhttp.responseText).message
                message.style='display: block;background-color: #fdf6ec;border-color: #faecd8;color: #E6A23C;';
                setTimeout(()=>{
                    message.style='display: none;';
                },2500)
            }
        }
    });
};

function Ajax(Request,API,data){
	return new Promise(function(resolve,reject){
		var xmlhttp = window.XMLHttpRequest ? new XMLHttpRequest() : new
		ActiveXObject('Microsoft.XMLHTTP');
		// console.log(data)
		var pairs = []
		for (var key in data) {
		    pairs.push(key + '=' + data[key])
		};
		var querystring = pairs.join('&')
		if( Request.toUpperCase() === 'POST' ) {
		   xmlhttp.open('POST', "http://erp_test.admin.htwig.com/" + API, true);
		   xmlhttp.setRequestHeader("Authorization","Bearer "+JSON.parse(window.localStorage.getItem('access_token')));
		   xmlhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		   xmlhttp.send(querystring);  // 要发送的参数，要转化为json字符串发送给后端，后端就会接受到json对象
		};
		if( Request.toUpperCase() === 'GET' ) {
			xmlhttp.open('GET', "http://erp_test.admin.htwig.com/" + API + '?' + querystring, true);
			xmlhttp.setRequestHeader("Authorization","Bearer "+JSON.parse(window.localStorage.getItem('access_token')));
			xmlhttp.send(null);
		}
		xmlhttp.addEventListener('readystatechange', function () {
		    if(xmlhttp.readyState == 4 && xmlhttp.status == 200){
		        if( JSON.parse(xmlhttp.responseText).code == 0 ) {
					resolve(JSON.parse(xmlhttp.responseText).data);
		        } else {
					reject(JSON.parse(xmlhttp.responseText).message);
		        }
		    }
		});
	})
}